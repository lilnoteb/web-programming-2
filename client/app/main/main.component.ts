import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../../components/services/user.service';
import {User} from '../../components/interfaces/User';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {UpdateUserComponent} from '../../components/modals/updateUser/updateUser.component';
//added
import {CreateUserComponent} from '../../components/modals/createUser/createUser.component';

@Component({
    selector: 'main',
    template: require('./main.html'),
    styles: [require('./main.scss')],
})
export class MainComponent implements OnInit {
    bsModalRef: BsModalRef;
    private values: string[];
    private valueToSquare: number;
    private users: User[];
    private input: string;

    static parameters = [HttpClient, BsModalService, UserService];
    constructor(private http: HttpClient, private modalService: BsModalService, private userService: UserService) {
        this.http = http;
        this.userService = userService;
        this.setData();
        this.getUserData();
    }

    private setData() {
        this.values = ['first', 'second', 'third'];
        this.valueToSquare = 4;
    }

    public getUserData() {
        this.userService.getAllUsers()
            .then(response => {
                this.users = response.users as User[];
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Something has gone wrong', error);
        return Promise.reject(error.message || error);
    }

    private incrementNumber() {
        this.valueToSquare++;
    }

    private updateUser(user) {
        const initialState = {
            user,
            title: `Update user ${user._id}`
        };
        this.bsModalRef = this.modalService.show(UpdateUserComponent, {initialState});
        this.bsModalRef.content.updatedUser.subscribe(updatedUser => {
            this.userService.updateUser(updatedUser)
                .then(() => {
                    this.getUserData();
                })
                .catch(err => {
                    console.error(err);
                });
        });
    }

	//added
	private createUser(user) {
        const initialState = {
            user,
            title: `Create user ${user._id}`
        };
        this.bsModalRef = this.modalService.show(CreateUserComponent, {initialState});
        this.bsModalRef.content.createdUser.subscribe(createdUser => {
            this.userService.createUser(createdUser)
                .then(() => {
                    this.getUserData();
                })
                .catch(err => {
                    console.error(err);
                });
        });
    }

    ngOnInit() {
    }
}
