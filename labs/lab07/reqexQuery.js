db.towns.find({name: {$regex: "new", $options: "$i"}}).pretty()
{
    "_id" : ObjectId("6029a9b929175192c193f7ff"),
    "name" : "New York",
    "population" : 22200000,
    "lastCensus" : ISODate("2016-07-01T00:00:00Z"),
    "famousFor" : [
    "the MOMA",
    "food",
    "Derek Jeter"
],
    "mayor" : {
    "name" : "Bill de Blasio",
        "party" : "D"
}
}
>
