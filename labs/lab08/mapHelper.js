mapHelper = function(city){
    var number = city.components.longitude + '',
        seen = [],
        result = [],
        i = number.length;

    while(i--) {
        seen[+number[i]] = 1;
    }

    for (var i = 0; i < 10; i++) {
        if (seen[i]) {
            result[result.length] = i;
        }
    }

    return result;
}

db.system.js.save({_id: 'mapHelper', value: mapHelper})
