/*eslint no-process-env:0*/

export const env = process.env.NODE_ENV;
export const port = process.env.PORT || 9000;
// List of user roles
export const userRoles = ['guest', 'user', 'admin'];

export const about = {
    email: 'lily.noteboom@du.edu',
    name: 'Lily Noteboom',
    copyright: 'Copyright 2021 Lily Noteboom'
}

export default {
    env,
    port,
    userRoles,
    about
};
