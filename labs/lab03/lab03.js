import fs from 'fs';
import zlib from 'zlib';

class lab03 {

  syncFileRead(filename) {
    let fileContents = fs.readFileSync(filename, { encoding: 'utf-8' });
    return fileContents;
  }

  asyncFileRead(filename, callback) {
    fs.readFile(filename, function (err, data) => {
      if (err) {
        return console.error(err);
      } else {
          callback(data.toString('utf-8'));
      }
    })
  }

    compressFileStream(inputFile, outputFile) {
        let fileContents = fs.createWrieStream(outputFile);
        fs.createReadStream(inputFile)
            .pipe(zlib.createGzip())
            .pipe(fs.createWriteStream(outputFile));

        console.log("File Compressed.");
        return fileContents;
    }

    decompressFileStream(inputFile, outputFile) {
      let fileContents = fs.createWriteStream(outputFile);
      fs.createReadStream(inputFile)
            .pipe(zlib.createGunzip())
            .pipe(fs.createWriteStream(outputFile));

        console.log("File Decompressed.");
        return fileContents;
    }

    listDirectoryContents(directoryName, callback) {
        fs.stat(directoryName, function (err, stats) {
            if (err) {
                return console.error(err);
            }
            return callback(stats);
        })

    }

export {lab03};
