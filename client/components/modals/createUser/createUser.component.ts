import {Component, EventEmitter, OnInit, Output, TemplateRef} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {User} from "../../interfaces/User";
import {HttpClient} from "@angular/common/http";
import {UserService} from "../../services/user.service";

@Component({
    selector: 'create-user-modal',
    template: require('./createUser.html')
})
export class CreateUserComponent implements OnInit {
    user: User;
    title: string;

    @Output() createdUser: EventEmitter<User> = new EventEmitter();

    static parameters = [BsModalRef];
    constructor(public bsModalRef: BsModalRef) {}

    createUser() {
        this.bsModalRef.hide();
        this.createdUser.emit(this.user);
    }

    ngOnInit() {
    }
}