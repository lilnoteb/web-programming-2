results = db.runCommand({
    mapReduce: 'cities',
    map: map,
    reduce: reduce,
    finalize: function(key, reducedValue) {
        return {total: reducedValue.count};
    },
    out: 'cities.report'
})
