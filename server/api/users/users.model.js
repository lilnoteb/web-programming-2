class User {
    users = [];

    find() {
        // Returns a list of all users
        return this.users;
    }

    findById(userId) {
        // Find user by Id
        // Returns user, or null if not present
        let foundUser = this.users.filter(function(user) {
            if(userId=== id) {
                return foundUser;
            }
        });
        if(foundUser.length > 0) {
            return foundUser[0];
        } else {
            return null;
        }
    }

    create(user) {
        // Create a new user
        // Return created user
        // Generate the id and overwrite any id that may be present in user
        let newUser = {
            name,
            address,
            age
        };
        return newUser;
        let id = uuidv4();
        let userId = id;
    }

    findOneAndUpdate(user) {
        // Find user and update
        // If user does not exist, create it using Id provided
        // Return true if user was updated, false if user was created
        let existingUser = find(user.params.id);
        if(existingUser) {
            existingUser.name = user.name;
            existingUser.address = user.address;
            existingUser.age = user.age;
            user.json(existingUser);
            return true;
        } else {
            user.create();
            return false;
        }
    }

    remove(user) {
        // Remove user if exists with the Id provided
        // Return true if removed
        // Return false if did user not exist
        let userIndex = this.user.map(function(user) {
            this.user.splice(user.id, 1);
            return true;
        })
            .indexOf(user.params.id);
        if(userIndex !== -1) {
            this.user.splice(user.id, 1);
            return true;
        } else {
            return false;
        }
    }
}

export default new User();
